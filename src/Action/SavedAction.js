export const SAVE_EVENT = 'SAVE_EVENT';

export const saveEvent = (value) => ({
    type: SAVE_EVENT,
    payLoad: value,
})