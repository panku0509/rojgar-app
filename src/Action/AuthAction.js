export const AUTH_DATA = 'AUTH_DATA';

export const authData = (value) => ({
    type: AUTH_DATA,
    payLoad: value,
})