export const data = [{
    id: 1,
    name:'Metallica Concert',
    venue:'Palace Grounds',
    type:'paid entry',
    paid: true,
    thumbNail: 'https://i.picsum.photos/id/450/200/150.jpg'
},
{
    id: 2,
    name:'Saree Exhibition',
    venue:'Malleswaram Grounds',
    type:'free entry',
    paid: false,
    thumbNail: 'https://i.picsum.photos/id/430/200/150.jpg'
},
{
    id: 3,
    name:'Wine tasting event',
    venue:'Brewery',
    type:'paid entry',
    paid: true,
    thumbNail: 'https://i.picsum.photos/id/431/200/150.jpg'
},
{
    id: 4,
    name:'Startups Meet',
    venue:'Kanteerava Indoor Stadium',
    type:'paid entry',
    paid: true,
    thumbNail: 'https://i.picsum.photos/id/432/200/150.jpg'
},
{
    id: 5,
    name:'Summer Noon Party',
    venue:'Kumara Park',
    type:'paid entry',
    paid: true,
    thumbNail: 'https://i.picsum.photos/id/433/200/150.jpg'
},
{
    id: 6,
    name:'Rock and Roll nights',
    venue:'Sarjapur Road',
    type:'paid entry',
    paid: true,
    thumbNail: 'https://i.picsum.photos/id/434/200/150.jpg'
},
{
    id: 7,
    name:'Barbecue Fridays',
    venue:'Whitefield',
    type:'paid entry',
    paid: true,
    thumbNail: 'https://i.picsum.photos/id/436/200/150.jpg'
},
{
    id: 8,
    name:'Summer workshop',
    venue:'Indiranagar',
    type:'paid entry',
    paid: true,
    thumbNail: 'https://i.picsum.photos/id/437/200/150.jpg'
},
{
    id: 9,
    name:'Impressions & Expressions',
    venue:'MG Road',
    type:'free entry',
    paid: false,
    thumbNail: 'https://i.picsum.photos/id/439/200/150.jpg'
},
{
    id: 10,
    name:'Italian carnival',
    venue:'Electronic City',
    type:'free entry',
    paid: false,
    thumbNail: 'https://i.picsum.photos/id/443/200/150.jpg'
}]
