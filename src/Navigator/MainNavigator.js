
import React from 'react';
import { connect } from 'react-redux'
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import EventDetail from '../Components/EventDetail';
import SavedEvents from '../Components/SavedEvents';
import ListView from '../Components/ListView';
import SignupScreen from '../Components/SignupScreen';

const Stack = createStackNavigator();

const MainNavigator = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="ListView" component={ListView} options={ListView.navigationOptions} />
        <Stack.Screen name="EventDetail" component={EventDetail} options={EventDetail.navigationOptions} />
        <Stack.Screen name="SavedEvents" component={SavedEvents} options={SavedEvents.navigationOptions} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
const SignUpNavigator = () => {
  return (
  <NavigationContainer>
    <Stack.Navigator>
      <Stack.Screen name="SignupScreen" component={SignupScreen} options={SignupScreen.navigationOptions} />
    </Stack.Navigator>
  </NavigationContainer>
  )
}

const App = (props) => {
  if(props.userName) {
    return (<MainNavigator />);
  } else {
    return (<SignUpNavigator />);
  }
}
const mapStateToProps = state => ({
    userName: state.AuthReducer.userName,
})
export default connect(mapStateToProps, null)(App);