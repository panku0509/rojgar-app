import React from 'react';
import { View, Text, ScrollView, Dimensions, TouchableOpacity, StyleSheet } from 'react-native';
import FastImage from 'react-native-fast-image'
import { data } from '../Constants/Data';
const { width } = Dimensions.get('window');

const GridView = ({navigation}) => {
    // const [modalVisible, setModalVisible] = useState(false);
    const cardClicked = (title, type, venue, thumbNail) => {
      navigation.navigate('EventDetail', { title, type, venue, thumbNail });
    }
    
    const renderItem = (keys) => {
        return (
          <TouchableOpacity
            style={styles.containerStyle}
            onPress={() => cardClicked(keys.name, keys.type, keys.venue, keys.thumbNail)}
            key={keys.id}
          >
                
                <FastImage
                    style={styles.imageStyle}
                    source={{
                        uri: keys.thumbNail,
                        headers: { Authorization: 'someAuthToken' },
                        priority: FastImage.priority.normal,
                    }}
                    resizeMode={FastImage.resizeMode.contain}
                />
            
                <View>
                    <Text style={styles.eventName}>
                        {keys.name}
                    </Text>
                    <Text>
                        {keys.venue}
                    </Text>
                    <Text style={styles.eventType}>
                        {keys.type}
                    </Text>
                </View>

            </TouchableOpacity>
        )
    }
    return (
      <ScrollView>
          <View style={styles.topContainer}>
              {data.map(keys => renderItem(keys))}
          </View>
      </ScrollView>
    );
}
const styles = StyleSheet.create({
  topContainer: { flexDirection: 'row', flexWrap: 'wrap', marginLeft: 16, marginTop: 16  },
  containerStyle: { marginBottom: 8, padding: 8, width: width * 0.45, borderWidth: 1, marginRight: 8 },
  imageStyle: { width: 150, height: 100 },
  eventName: { fontSize: 20 },
  eventType: { fontSize: 18, fontWeight: '900', color: '#2CC40A'}
})
export default GridView;
