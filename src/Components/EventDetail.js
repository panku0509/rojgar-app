import React from 'react';
import { View, Text, ScrollView, Dimensions, TouchableOpacity, StyleSheet } from 'react-native';
import { connect } from 'react-redux';
import FastImage from 'react-native-fast-image';
import Swipeable from 'react-native-gesture-handler/Swipeable';
import SavedEvents from '../Components/SavedEvents';
import { saveEvent } from '../Action/SavedAction';

const { width } = Dimensions.get('window');

const EventDetail = ({ route, saveEvent, eventList, navigation }) => {
    const { title, type, venue, thumbNail } = route.params;
    const storeData = () => {
        saveEvent(route.params)
    }
    const renderRightActions = () => <></>
    const renderLeftActions = () => <SavedEvents navigation={navigation} />
    return (
      <Swipeable renderLeftActions={renderLeftActions} renderRightActions={renderRightActions}>
        <ScrollView>
          <FastImage
              style={styles.imageStyle}
              source={{
                  uri: thumbNail,
                  headers: { Authorization: 'someAuthToken' },
                  priority: FastImage.priority.normal,
              }}
              resizeMode={FastImage.resizeMode.contain}
          />
          <View style={styles.textContainer}>
            <Text style={styles.eventName}>
              {title}
            </Text>
          </View>
          <View style={styles.textContainer}>
            <Text style={styles.eventVenue}>
              {venue}
            </Text>
          </View>
          <View style={styles.textContainer}>
            <Text style={styles.eventVenue}>
              {type}
            </Text>
          </View>
          {eventList.findIndex(keys => keys.title === title) === -1 ?
            <TouchableOpacity onPress={storeData} style={styles.trackButton} >
              <Text style={styles.buttonText}>
                {'Track event'}
              </Text>
            </TouchableOpacity>
            :
            <View style={styles.alreadyTrakingButton} >
              <Text style={styles.buttonText}>
                {'Already tracking this event'}
              </Text>
            </View>
          }

        </ScrollView>
      </Swipeable>
    );
}

const mapDispatchToProps = dispatch => ({
  saveEvent: value => dispatch(saveEvent(value))
})
const mapStateToProps = state => ({
  eventList: state.SaveEvents.eventList,
})

EventDetail.navigationOptions = ({ navigation, route }) => {
  return {
    title: route.params?.title ?? null,
  }
};

const styles = StyleSheet.create({
  imageStyle: { width: width - 32, height: 300, marginLeft: 16 },
  buttonText: {color: 'white', fontWeight: 'bold', fontSize: 18},
  eventVenue: { color: '#0D0C04', fontSize: 20, fontWeight: 'bold' },
  eventName: { color: '#0D0C04', fontSize: 24, fontWeight: 'bold' },
  textContainer: { flexDirection: 'row', justifyContent: 'center', alignItems: 'center' },
  trackButton: {backgroundColor: '#FF8633', padding: 10, borderRadius: 8, flexDirection: 'row', justifyContent:'center', alignItems: 'center', margin: 24 },
	alreadyTrakingButton : {backgroundColor: '#CFCECD', padding: 10, borderRadius: 8, flexDirection: 'row', justifyContent:'center', alignItems: 'center', margin: 24 }
});

export default connect(mapStateToProps, mapDispatchToProps)(EventDetail)
