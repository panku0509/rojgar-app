import React, {useState} from 'react';
import { connect } from 'react-redux'
import { authData } from '../Action/AuthAction';
import { View, TextInput, StyleSheet, Dimensions, Text, TouchableOpacity } from 'react-native';

const { width } = Dimensions.get('window');

const SignupScreen = ({ authData}) => {
  const [value, onChangeText] = useState('')
  const onPress = () => {
    authData(value)
  }
  return (
        <View style={styles.insideContainer}>
          <Text style={styles.nameText}>
            Enter Your Name
          </Text>
          <TextInput
            style={styles.TextInputStyle}
            onChangeText={text => onChangeText(text)}
            placeholder="Enter your name.."
            value={value}
          />
            <TouchableOpacity onPress={onPress} style={styles.signButtom} >
              <Text style={styles.buttonText}>
                {'Let me in'}
              </Text>
            </TouchableOpacity>
        </View>
  );
};



const mapDispatchToProps = dispatch => ({
  authData: value => dispatch(authData(value))
})

export default connect(null, mapDispatchToProps)(SignupScreen)

const styles = StyleSheet.create({
  nameText: {
    margin: 16,
    marginLeft: 16,
    fontSize: 24,
  },
  insideContainer: {
    borderRadius: 4,
    flex: 1,
    backgroundColor: 'white',
  },
  buttonText: { color: 'white', fontWeight: 'bold', fontSize: 18 },
  TextInputStyle: { height: 40, borderColor: '#0D0C04', padding: 10, fontSize: 20 },
  signButtom: { backgroundColor: '#FF8633', padding: 10, borderRadius: 8, flexDirection: 'row', justifyContent:'center', alignItems: 'center', margin: 24 }
});
