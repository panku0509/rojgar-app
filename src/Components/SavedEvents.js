import React from 'react';
import { View, Text, Dimensions, FlatList, TouchableOpacity, StyleSheet } from 'react-native';
import FastImage from 'react-native-fast-image'
import { connect } from 'react-redux'
const { width } = Dimensions.get('window');

const SavedEvents = ({ eventList, navigation }) => { 
    const cardClicked = (title, type, venue, thumbNail) => {
        navigation.navigate('EventDetail', { title, type, venue, thumbNail });
    }  
    const renderItem = ({item}) => {
        return (
            <TouchableOpacity 
                style={styles.containerStyle}
                onPress={() => cardClicked(item.title, item.type, item.venue, item.thumbNail)}
            >
                <FastImage
                    style={styles.imageStyle}
                    source={{
                        uri: item && item.thumbNail,
                        headers: { Authorization: 'someAuthToken' },
                        priority: FastImage.priority.normal,
                    }}
                    resizeMode={FastImage.resizeMode.contain}
                />
                {item && 
                    <View>
                        <Text style={styles.eventName}>
                            {item.title}
                        </Text>
                        <Text>
                            {item.venue}
                        </Text>
                    </View>
                }
            </TouchableOpacity>
        )
    }
    return (
            <View style={styles.topContainer}>
                <View style={styles.savedEventContainer}>
                    {eventList.length > 0 ? 
                        <Text style={styles.savedEventText}>
                            Saved Event
                        </Text>
                        :
                        <Text style={styles.savedEventText}>
                            Not Saved yet
                        </Text>
                    }   
                </View>
                {Array.isArray(eventList) && eventList.length > 0 &&
                    <FlatList
                        data={eventList}
                        renderItem={renderItem}
                    />
                }
            </View>
    );
}

const styles = StyleSheet.create({
    containerStyle: {flexDirection: 'row', marginBottom: 8, borderColor: '#030303', borderWidth: 1, marginHorizontal: 8 },
    savedEventContainer: { flexDirection: 'row', justifyContent: 'center', alignItems: 'center', marginVertical: 16 },
    imageStyle: { width: 150, height: 100 },
    savedEventText: {fontSize: 20, fontWeight: 'bold'},
    eventName: { fontSize: 20 },
    topContainer: {width, flex: 1}
})

const mapStateToProps = state => ({
    eventList: state.SaveEvents.eventList,
})
export default connect(mapStateToProps, null)(SavedEvents);
