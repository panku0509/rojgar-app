import React, { useState, useEffect } from 'react';
import { View, Text, FlatList, Switch, TouchableOpacity, StyleSheet } from 'react-native';
import FastImage from 'react-native-fast-image'
import Swipeable from 'react-native-gesture-handler/Swipeable';
import SavedEvents from './SavedEvents';
import { data } from '../Constants/Data';
import GridView from './GridView';


const ListView = ({navigation}) => {
    const [isEnabled, setIsEnabled] = useState(false);
    const toggleSwitch = () => {
      setIsEnabled(previousState => !previousState);
    };
    const cardClicked = (title, type, venue, thumbNail) => {
      navigation.navigate('EventDetail', { title, type, venue, thumbNail });
    }
    useEffect(() => {
      navigation.setParams({
      isEnabled: isEnabled
      })
    }, [isEnabled]);
    useEffect(() => {
      navigation.setParams({
      toggleSwitch: toggleSwitch,
      isEnabled: isEnabled
    })
    },[])
    const renderRightActions = () => <></>
    const renderLeftActions = () => <SavedEvents navigation={navigation} />
    const renderItem = ({item}) => {
        return (
            <TouchableOpacity 
              style={styles.containerStyle}
              onPress={() => cardClicked(item.name, item.type, item.venue, item.thumbNail)}
            >
                <FastImage
                    style={styles.imageStyle}
                    source={{
                        uri: item.thumbNail,
                        headers: { Authorization: 'someAuthToken' },
                        priority: FastImage.priority.normal,
                    }}
                    resizeMode={FastImage.resizeMode.contain}
                />
                <View>
                    <Text style={styles.eventName}>
                        {item.name}
                    </Text>
                    <Text>
                        {item.venue}
                    </Text>
                    <Text>
                        {item.type}
                    </Text>
                </View>
            </TouchableOpacity>
        )
    }
    return (
      <View>
          <Swipeable renderLeftActions={renderLeftActions} renderRightActions={renderRightActions}>

            {isEnabled ? (
              <View style={styles.listContainer}>
                <FlatList
                data={data}
                renderItem={renderItem}
                />
              </View>
            )
              :
            (
              <GridView
                navigation={navigation}
              />
            )
            }
          </Swipeable>
      </View>
    );
}
ListView.navigationOptions = ({ route }) => {
  return {
    title: route.params?.isEnabled ?? null ? 'List View' : 'Grid View',
    headerRight: () => (
      <View>
        <Switch
         trackColor={{ false: "#767577", true: "#81b0ff" }}
         thumbColor={route.params?.isEnabled ?? null ? "#f5dd4b" : "#f4f3f4"}
         ios_backgroundColor="#3e3e3e"
         onValueChange={route.params?.toggleSwitch ?? {}}
         value={route.params?.isEnabled ?? null}
       />
      </View>
    )
  }
};


const styles = StyleSheet.create({
  containerStyle: {flexDirection: 'row', marginBottom: 8, borderColor: '#030303', borderWidth: 1, marginHorizontal: 8 },
  imageStyle: { width: 150, height: 100 },
  eventName: { fontSize: 20 },
  listContainer: { marginTop: 16 }
})

export default ListView;
