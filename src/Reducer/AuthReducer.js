
import { AUTH_DATA } from '../Action/AuthAction';

const initialState = {
	userName: '',
};

const AuthReducer = (state = initialState, action) => {
    switch (action.type) {
      case AUTH_DATA:
        return {
            ...state,
            userName: action.payLoad,
        }
      default:
        return state
    }
  }
  
  export default AuthReducer