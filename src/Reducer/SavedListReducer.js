
import { SAVE_EVENT } from '../Action/SavedAction';

const initialState = {
	eventList: [],
};

const SaveEvents = (state = initialState, action) => {
    switch (action.type) {
      case SAVE_EVENT:
        // state.eventList.push(action.payLoad);
        return {
            ...state,
            eventList: state.eventList.concat(action.payLoad),
        }
      default:
        return state
    }
  }
  
  export default SaveEvents