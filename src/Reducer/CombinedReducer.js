import { combineReducers } from 'redux'
import AuthReducer from '../Reducer/AuthReducer';
import SaveEvents from '../Reducer/SavedListReducer';

const rootReducer = combineReducers({
    AuthReducer,
    SaveEvents
})

export default rootReducer;